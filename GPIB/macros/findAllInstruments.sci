// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
// Copyright (C) 2015 - Scilab Enterprises - Sylvain GENIN
//
// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
//
//
function [status, DeviceConnect] = findAllInstruments()

    DeviceConnect = [];
    NbDevice = 1;

    [status, defaultRM] = viOpenDefaultRM();
    if status < viGetDefinition("VI_SUCCESS") then
        viClose (defaultRM);
        return;
    end

    [status, fList, nbInstruments, instrDescriptor] = viFindRsrc(defaultRM, "?*INSTR");

    if status < viGetDefinition("VI_SUCCESS") then
        viClose (defaultRM);
        return;
    end

    DeviceConnect(NbDevice) = instrDescriptor;
    NbDevice = NbDevice+1 ;

    [status, instrDescriptor] = viFindNext (fList);

    while status == viGetDefinition("VI_SUCCESS")
    
        DeviceConnect(NbDevice) = instrDescriptor;
        NbDevice = NbDevice+1 ;
        [status, instrDescriptor] = viFindNext (fList);
    
    end

    viClose (defaultRM);

endfunction
