// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
// Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
//
// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
//
//
function [status, buf] = viSPrintf(session, str, varargin)
  fmt_str = sprintf(str, varargin(:));
  [status, buf] = viSPrint(session, fmt_str);
endfunction
