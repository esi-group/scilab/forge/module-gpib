// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
function cleanmacros()

    libpath = get_absolute_file_path("cleanmacros.sce");

    binfiles = ls(libpath+"/*.bin");
    for i = 1:size(binfiles,"*")
        mdelete(binfiles(i));
    end

    mdelete(libpath+"/names");
    mdelete(libpath+"/lib");
endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack

