// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode

function subdemolist = demo_gateway()
    demopath = get_absolute_file_path("visa.dem.gateway.sce");

    subdemolist = ["Find All instruments"                    ,"FindRsrc.dem.sce" ;..
                   "TCP / IP"                                ,"TCPIP.dem.sce"       ];

    subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
