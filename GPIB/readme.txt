readme.txt of the Scilab VISA

Note
--------

the Dlls is not embedded why ?
the Dlls NI_VISA need the environment variable VXIPNPPATH, it is provided in the NI software.
http://www.ni.com/download/ni-visa-14.0/4722/en/


Bibliography
------------

 * "VISA" is the module for communication with NI or Agilent devices

Authors
-------

 * Copyright (c) 2014-2015 - Scilab Enterprises - Sylvain GENIN
 * Copyright (c) 2014-2016 - Scilab Enterprises - Clément David
 * Copyright (c) 2015 - Scilab Enterprises - Simon MARCHETTO

ATOMS
-----

http://atoms.scilab.org/toolboxes/visa/

Licence
-------

This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
http://creativecommons.org/licenses/by-nd/4.0/legalcode

