function cflags = getCompilationFlags()
   
    cflags = "";
    [a, opt] = getversion();
    if getos()=="Windows" then
        if win64() then
          cflags = ['""$(VXIPNPPATH64)Win64\Include""'];
        else
          cflags = ['""$(VXIPNPPATH)WinNT\Include""'];
        end
    end
    
endfunction


function ldflags = getLinkFlags()

    ldflags = "";
    [a, opt] = getversion();
    if getos()=="Windows" then
        if win64() then
            ldflags = ["""$(VXIPNPPATH64)Win64\Lib_x64\msc\visa64.lib"""];
        else
            ldflags = ["""$(VXIPNPPATH)WinNT\lib\msc\visa32.lib"""];
        end
    else
        ldflags = " -lvisa ";
    end

endfunction

//Header
// lib_name = "GPIB";
// ldflags = [""];
// cflags = [];//"-I " + builddir;
// ldflags = [];
// exec("buildflags.sci");
// libs  = "";
// ldflags = ldflags +  getLinkFlags();
// if getos()=="Windows" then
    // files = "GPIB_wrap.c";
    // cflags = cflags + " -I "+  getCompilationFlags();
// else
    // files = fullpath("GPIB_wrap.c");
    // cflags = "-g " + cflags + getCompilationFlags();
// end

//Footer
// if ~isempty(table) then
  // if getos() == "Windows" then
    // libfilename = 'GPIB' + getdynlibext();
  // else
    // libfilename = 'libGPIB' + getdynlibext();
  // end
  // ierr = execstr("ilib_build(''GPIB'', table, files, libs, [], ldflags, cflags);", 'errcatch');
  // if ierr <> 0 then
    // err_msg = lasterror();
  // elseif ~isfile(libfilename) then
    // ierr = 1;
    // err_msg = 'Error while building library ' + libfilename;
  // elseif ~isfile('loader.sce') then
    // ierr = 1;
    // err_msg = 'Error while generating loader script loader.sce.';
  // end
// end
// cd(originaldir);
// if ierr <> 0 then
  // error(ierr, err_msg);
// end