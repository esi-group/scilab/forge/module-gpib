%typemap(arginit, noblock=0, fragment="SWIG_SciString_AsCharPtrAndSize") (ViBuf buf, ViUInt32 cnt)
{
  char* databuf = 0;
  char* buftempo = NULL;
  int size_buf = 0;
}

%typemap(in, noblock=0, fragment="SWIG_SciString_AsCharPtrAndSize") (ViBuf buf, ViUInt32 cnt)
{
    if (SWIG_SciString_AsCharPtrAndSize(pvApiCtx, $input, &databuf, NULL, &size_buf, SWIG_Scilab_GetFuncName()) == SWIG_OK) {
	buftempo = strchr(databuf,'\\');
	if((buftempo != NULL)&&((*(buftempo+1)=='n')||(*(buftempo+1)=='N')))
	{
		size_buf = (buftempo - databuf);
	}
	else
	{
		size_buf = strlen(databuf);
	}

	buftempo = (char*)calloc((size_buf + 2),sizeof(char));
	strncpy_s(buftempo, (size_buf + 2), databuf, size_buf);
	FREE(databuf);
	$1 = (ViBuf)buftempo;
	$2 = size_buf + 1;
	$1[size_buf] = '\n';

    }
    else {
        return SWIG_ERROR;
    }
}

%typemap(freearg, noblock=1) (ViBuf buf, ViUInt32 cnt)
{  
}

//ViPBuf buf typemaps
%typemap(arginit, noblock=0) (ViPBuf buf)
{
  $1 = (ViBuf) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") (ViPBuf buf)
{
   %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) (ViPBuf buf)
{  
}

%typemap(in, numinputs=0, noblock=1) (ViPBuf buf) {
}

%typemap(in, noblock=1, fragment=SWIG_AsVal_frag(int)) (ViPBuf buf, ViUInt32 cnt) (int bufsize = 0)
{    
  if (SWIG_AsVal_int($input, &bufsize) != SWIG_OK) {
    return SWIG_ERROR;
  }
  $2 = (ViUInt32) bufsize;
  $1 = (ViBuf) calloc((bufsize+1) , sizeof(char));
}


%typemap(arginit, noblock=1) (ViPBuf buf, ViUInt32 cnt)
{  
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") (ViPBuf buf, ViUInt32 cnt)
{  
  %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) (ViPBuf buf, ViUInt32 cnt)
{  
}


%typemap(in, numinputs=0, noblock=1) (ViPBuf buf) {
}


%typemap(arginit) (ViPBuf buf, ViString writeFmt)
{  
  $1 = (ViBuf) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(freearg, noblock=1) (ViPBuf buf, ViString writeFmt)
{  
}

// ViChar desc[] typemaps

%typemap(in, numinputs=0, noblock=1) ViChar desc[] {
}

%typemap(arginit, noblock=1) ViChar desc[] {
  $1 = (ViChar*) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") ViChar desc[] 
{
  %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) ViChar desc[] 
{  
}



// ViChar rsrcClass[] typemaps

%typemap(in, numinputs=0, noblock=1) ViChar rsrcClass[] {
}

%typemap(arginit, noblock=1) ViChar rsrcClass[] {
  $1 = (ViChar*) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") ViChar rsrcClass[] 
{
  %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) ViChar rsrcClass[] 
{  
}

// ViChar expandedUnaliasedName[] typemaps

%typemap(in, numinputs=0, noblock=1) ViChar expandedUnaliasedName[] {
}

%typemap(arginit, noblock=1) ViChar expandedUnaliasedName[] {
  $1 = (ViChar*) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") ViChar expandedUnaliasedName[] 
{
  %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) ViChar expandedUnaliasedName[]
{  
}

// ViChar aliasIfExists[] typemaps

%typemap(in, numinputs=0, noblock=1) ViChar aliasIfExists[] {
}

%typemap(arginit, noblock=1) ViChar aliasIfExists[] {
  $1 = (ViChar*) calloc((VI_FIND_BUFLEN+1), sizeof(char));
}

%typemap(argout, noblock=1, fragment="SWIG_FromCharPtr") ViChar aliasIfExists[] 
{
  %set_output(SWIG_FromCharPtr((const char *)$1));
}

%typemap(freearg, noblock=1) ViChar aliasIfExists[] 
{  
}
