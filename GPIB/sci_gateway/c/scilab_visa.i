/* File : scilab_visa.i */
%module scilab_visa

%{
#include "visatype.h"
#include "visa.h"
%}

%include "rename_scilab_visa.i"

%scilabconst(1);

%include "visatype.h"

#define _VI_FUNC

%include "pointeur_scilab_visa.i"
%include "ignore_scilab_visa.i"


%{
#include "stdint.h"
%}

%include "typemap_scilab_visa.i"

%include "visa.h"

%inline %{
ViStatus viPrint(ViSession vi, ViString writeFmt)
{
   return viPrintf(vi, writeFmt);
}
ViStatus viSPrint(ViSession vi, ViPBuf buf, ViString writeFmt)
{
   return viSPrintf(vi, buf, writeFmt);
}
ViStatus viScan(ViSession vi, ViString readFmt)
{
   return viScanf(vi, readFmt);
}
ViStatus viSScan(ViSession vi, ViPBuf buf, ViString readFmt)
{
   return viSScanf(vi, buf, readFmt);
}
ViStatus viQuery(ViSession vi, ViString writeFmt, ViString readFmt)
{
   return viQueryf(vi, writeFmt, readFmt);
}
%}




