// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2015 - Scilab Enterprises - Sylvain GENIN
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
// http://forge.scilab.org/index.php/p/module-gpib/issues/1555/#preview
[status, defaultRM] = viOpenDefaultRM();
[status, instr] = viOpen(defaultRM, "TCPIP0::ftp.ni.com::21::SOCKET", viGetDefinition("VI_NULL"), viGetDefinition("VI_NULL"));
[status, count] = viWrite(instr, "*IDN?");
[status, bufferOut, count] = viRead(instr, 10);
assert_checkequal(count,10);
assert_checkequal(bufferOut , "220 ftp.ni");
assert_checkequal(status,1073676294);
viClose (instr);
viClose (defaultRM);