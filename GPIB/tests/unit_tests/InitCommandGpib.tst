// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
[status, defaultRM] = viOpenDefaultRM ();
assert_checkequal(status , 0);
[status, instr] = viOpen (defaultRM, "TCPIP0::ftp.ni.com::21::SOCKET", viGetDefinition("VI_NULL"),viGetDefinition("VI_NULL"));
assert_checkequal(status , 0);
viClose (instr);
viClose (defaultRM);