<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
 * Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
 *
 * Copyright 2015-2016 Scilab Enterprises
 * This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
 * http://creativecommons.org/licenses/by-nd/4.0/legalcode
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="viOpenDefaultRM" xml:lang="en">
    <refnamediv>
        <refname>viOpenDefaultRM</refname>
        <refpurpose>This function returns a session to the Default Resource Manager resource.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>[status, sesn] = viOpenDefaultRM()</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>status</term>
                <listitem>
                    <para>contains the return code of the operation.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>sesn</term>
                <listitem>
                    <para>unique logical identifier to a Default Resource Manager session.</para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>The <link linkend="viOpenDefaultRM">viOpenDefaultRM()</link> function must be called before any VISA operations can be invoked. The first call to this function initializes the VISA system, including the Default Resource Manager resource, and also returns a session to that resource. Subsequent calls to this function return unique sessions to the same Default Resource Manager resource. When a Resource Manager session is passed to <link linkend="viClose">viClose()</link>, not only is that session closed, but also all find lists and device sessions (which that Resource Manager session was used to create) are closed.</para>
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example">
        [status, defaultRM] = viOpenDefaultRM();

        viClose(defaultRM);
        </programlisting>
    </refsection>
</refentry>
