// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode

function cleaner_help()
    path = get_absolute_file_path("cleaner_help.sce");
    langdirs = dir(path);
    langdirs = langdirs.name(langdirs.isdir);

    for l = 1:size(langdirs, "*")
        masterfile = fullpath(path + filesep() + langdirs(l) + "/master_help.xml");
        mdelete(masterfile);

        jarfile = fullpath(path + "/../jar/scilab_" + langdirs(l) + "_help.jar");
        mdelete(jarfile);

        tmphtmldir = fullpath(path + "/" + langdirs(l) + "/scilab_" + langdirs(l) + "_help");
        rmdir(tmphtmldir, "s");
    end
endfunction

cleaner_help();
clear cleaner_help;
